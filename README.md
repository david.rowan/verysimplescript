# verySimpleScript

Very simple script. Just a hello, world! script for testing downloading and passing a parameter.

## Usage:
Load as either a script or module. Call the function as:
    ReallySimple -word "YOUR WORD HERE"
Will return "Hello, YOUR WORD HERE!".
Without a parameter will simply return "Hello, world!"
